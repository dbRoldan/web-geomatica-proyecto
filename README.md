# Universidad Distrital Francisco José de Caldas
## Facultad Ingenieria
### Geomatica I y II
![UDistrital](public/images/udistrital_esc.png "Universidad Distrital Francisco José de Caldas")
#### Creador:
* Daissi Bibiana Gonzalez Roldan   **Codigo:** 20152020108
* Brian Alfonso Rodriguez **Codigo:** 20151020600
#### Descripcion: Proyecto Final - Geomatica
Denominado **Bogografiando**, es un proyecto cuyo objetivo es proveer informacion respecto a las visitas de los usuarios, y consecuentemente dar un acercamiento mas acertado a las recomendaciones que pueden darse dentro del turismo.
#### Herramientas:
* Bootstrap
* HTML5
* CSS3
* Story Maps
* Survey 123
#### Link:
Para acceder a una la pagina web de click aqui:
[Bogografiando-Web](https://dbroldan.gitlab.io/web-geomatica-proyecto " Pagina Web - Bogografiando")

#### Vista Previa:
![Web](public/images/web.png "Bogografiando")

#### Licencia:
GNU License
